
public class ProgramadeEntrada {

	public static void main(String[] args) {
		EntradaDoCinema  objeto1= new EntradaDoCinema("A lora","13","a","13 d",100.0,true);
		EntradaDoCinema  objeto2= new EntradaDoCinema("A lora","13","a","11 d",20.0,true);
		EntradaDoCinema  objeto3= new EntradaDoCinema("A lora","13","a","12 d",20.0,true);
		
		objeto1.realizarVenda();
		objeto2.realizarVenda();
		objeto3.realizarVenda();
		
		System.out.println("\n");
		
		objeto1.calcularValorDesconto(2011, 12, 07);
		objeto2.calcularValorDesconto(1998, 12, 07);
		objeto3.calcularValorDesconto(2007, 9, 24);
		
		System.out.println("\n");
		
		objeto1.calcularValorDesconto(2006, 7, 12, 121212);
		objeto2.calcularValorDesconto(1998, 12, 07, 123123123);
		objeto3.calcularValorDesconto(2007, 9, 24,11231231);
		
		System.out.println("\n");
		
		System.out.println(objeto1.toString());
		System.out.println("\n");
		System.out.println(objeto2.toString());
		System.out.println("\n");
		System.out.println(objeto3.toString());
		System.out.println("\n");
		System.out.println(objeto1.toString());
		
	}

}
