package br.ufc.quixada.EC.poo.classesModelo;

public  class ClientePessoaJuridica extends Cliente{

	private String cnpj;
	
	public ClientePessoaJuridica(String nome, String endereco,String cnpj) {
		super(nome, endereco);
		this.cnpj=cnpj;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	@Override
	public String toString() {
		return "ClientePessoaJuridica [cnpj=" + cnpj + "]";
	}
	
}
