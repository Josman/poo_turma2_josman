 package br.ufc.quixada.EC.poo.classesModelo;

public  class ServicosGerais extends Funcionario{
	
	public ServicosGerais(String nome,String cpf, String matricula, float salario) {
		super(nome,cpf, matricula, salario);
	}
	
	public void limpar(){
		darBonificacao();
	}
	
	@Override
	public String toString() {
		return "ServicosGerais [getCpf()=" + getCpf() + ", getMatricula()=" + getMatricula() + ", getSalario()="
				+ getSalario() + ", toString()=" + super.toString() + ", getNome()=" + getNome() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
	@Override
	public void darBonificacao() {
		setSalario(getSalario()+3);
		
	}
}
