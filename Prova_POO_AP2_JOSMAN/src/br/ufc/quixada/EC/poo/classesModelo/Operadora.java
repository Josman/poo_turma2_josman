package br.ufc.quixada.EC.poo.classesModelo;

import java.util.List;

import br.ufc.quixada.EC.poo.interfaces.Imprimivel;

public class Operadora implements Imprimivel{
	private int codOperadora;
	private String nome;
	private List<Contrato> contratos;
	private List<Cliente> clientes;
	
	public Operadora(){
		
	}
	public Operadora(int cod, String nome, Contrato c,Cliente client){
		
	}
	// Gets e Sets
	public int getCodOperadora() {
		return codOperadora;
	}
	public void setCodOperadora(int codOperadora) {
		this.codOperadora = codOperadora;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Contrato> getContratos() {
		return contratos;
	}
	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public void cadastrarContratoCliente(Contrato contrato){
		clientes.add(contrato.getCliente());
		contratos.add(contrato);
	}
	
	@Override
	public void mostrarContratos() {
		System.out.println(contratos.toString());
		System.out.println(clientes.toString());
	}
	
	
	
	
}
