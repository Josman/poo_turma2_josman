package br.ufc.quixada.EC.poo.classesModelo;

public class Vendedor extends Funcionario{
	
	public Vendedor(String nome,String cpf, String matricula, float salario) {
		super(nome,cpf, matricula, salario);
	}
	
	public void realizarVenda(float valorContrato){
		setSalario((float) (getSalario()+(valorContrato*0.5)));
		darBonificacao();
	}
	@Override
	public String toString() {
		return "Vendedor [getCpf()=" + getCpf() + ", getMatricula()=" + getMatricula() + ", getSalario()="
				+ getSalario() + ", toString()=" + super.toString() + ", getNome()=" + getNome() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
	@Override
	public void darBonificacao() {
		setSalario((float) (getSalario()+(getSalario()*0.05)));
	}
}
