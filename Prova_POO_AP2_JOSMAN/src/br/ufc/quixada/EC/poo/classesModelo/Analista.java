package br.ufc.quixada.EC.poo.classesModelo;

public  class Analista extends Funcionario{
	
	public Analista(String nome,String cpf, String matricula, float salario) {
		super(nome,cpf, matricula, salario);
	}
	public void processarContrato(Operadora op, Contrato c){
		
		Operadora operadora= new Operadora();
	
		operadora.cadastrarContratoCliente(c);
		
		darBonificacao();
	}
	@Override
	public String toString() {
		return "Analista [getCpf()=" + getCpf() + ", getMatricula()=" + getMatricula() + ", getSalario()="
				+ getSalario() + ", toString()=" + super.toString() + ", getNome()=" + getNome() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
	@Override
	public void darBonificacao() {
		setSalario((float) (getSalario()+(getSalario()*0.02)));
	}
}
