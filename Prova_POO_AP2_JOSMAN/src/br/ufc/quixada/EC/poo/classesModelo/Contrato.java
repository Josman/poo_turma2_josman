package br.ufc.quixada.EC.poo.classesModelo;

import java.time.LocalDate;

public class Contrato {
	
	private int codContrato;
	private Cliente cliente;
	private Funcionario vendedor;
	private LocalDate datainicio;
	private float valorContrato;
	
	public Contrato(int cod, Cliente c,Funcionario v,LocalDate data, float valor){
		this.codContrato=cod;
		this.cliente=c;
		this.vendedor=v;
		this.datainicio=data;
		this.valorContrato=valor;
	}
	// Gets e Sets
	public int getCodContrato() {
		return codContrato;
	}
	public void setCodContrato(int codContrato) {
		this.codContrato = codContrato;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Funcionario getVendedor() {
		return vendedor;
	}
	public void setVendedor(Funcionario vendedor) {
		this.vendedor = vendedor;
	}
	public LocalDate getDatainicio() {
		return datainicio;
	}
	public void setDatainicio(LocalDate datainicio) {
		this.datainicio = datainicio;
	}
	public float getValorContrato() {
		return valorContrato;
	}
	public void setValorContrato(float valorContrato) {
		this.valorContrato = valorContrato;
	}
	
	
	@Override
	public String toString() {
		return "Contrato [codContrato=" + codContrato + ", cliente=" + cliente + ", vendedor=" + vendedor
				+ ", datainicio=" + datainicio + ", valorContrato=" + valorContrato + "]";
	}
	
	
	
	
}
