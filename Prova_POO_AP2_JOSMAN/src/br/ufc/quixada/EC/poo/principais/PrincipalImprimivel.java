package br.ufc.quixada.EC.poo.principais;

import java.time.LocalDate;

import br.ufc.quixada.EC.poo.classesModelo.ClientePessoaFisica;
import br.ufc.quixada.EC.poo.classesModelo.ClientePessoaJuridica;
import br.ufc.quixada.EC.poo.classesModelo.Contrato;
import br.ufc.quixada.EC.poo.classesModelo.Operadora;
import br.ufc.quixada.EC.poo.classesModelo.Vendedor;


public class PrincipalImprimivel {
	public static void main(String[] args) {
		LocalDate ld = null;
		
		Vendedor v1 = new Vendedor("carlos","5555555556","391178", 1000);
		Vendedor v2 = new Vendedor("erlandio","6666666664","401051", 1200);
		Vendedor v3 = new Vendedor("caique","4444444445","291118", 1500);
		
		ClientePessoaFisica c1=new ClientePessoaFisica("joao","rua 12","1234567890");
		ClientePessoaFisica c2=new ClientePessoaFisica("raimundo","rua 13","9876543210");
		ClientePessoaJuridica c3=new ClientePessoaJuridica("ltda","distrito industrial","0660606606600");
		
		
		Contrato contrato1=new Contrato(1,c1,v1,ld,100);
		Contrato contrato2=new Contrato(2,c2,v2,ld,500);
		Contrato contrato3=new Contrato(3,c3,v3,ld,1000);
		
		Operadora operadora=new Operadora();
		
		operadora.cadastrarContratoCliente(contrato1);
		operadora.cadastrarContratoCliente(contrato2);
		operadora.cadastrarContratoCliente(contrato3);
		 
		operadora.mostrarContratos();
		
		

		
	}

	

}
