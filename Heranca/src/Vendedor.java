
public class Vendedor extends Empregado{
	private double valorVendas;
	private double comiss�o;
	
	public double getValorVendas() {
		return valorVendas;
	}
	public void setValorVendas(double valor) {
		this.valorVendas=valor;
	}
	public double getComiss�o() {
		return comiss�o;
	}
	public void setComiss�o(double comissao) {
		this.comiss�o=comissao;
	}
	
	public double calcularVendedor() {
		comiss�o=valorVendas+(valorVendas*comiss�o/100);
		return calcularSalario()+comiss�o;
	}
}
